# ansible-tools

Collection of useful tools for ansible

## Bootstraping

The script `bootstrap.sh` should install ansible 2 on popular distros.

### Usage

Just have to run it

### Info

* This script will ever prefere distro package installation if available.
* If distro is not known or ansible package not available, it will try to install it from pip.
* If the distro package is too old, the script should rollback on pip installation.

### Tests

At 2017, April, 1th, the bootstrap has been tested and works on :

 - `centos-6`
 - `centos-7`
 - `scientific-linux-6`
 - `scientific-linux-7`
 - `ubuntu-precise`
 - `ubuntu-trusty`
 - `ubuntu-xenial`
 - `debian-wheezy`
 - `debian-jessie`
 - `gentoo`
 - `archlinux`
